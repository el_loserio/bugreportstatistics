#!/usr/bin/python

import os
import subprocess

command = ['R']
env = os.environ.copy()
env['R_RESULTS'] = os.path.join('..', 'data')
cwd = '.'
subprocess.call(command, env=env, cwd=cwd, shell=True)
